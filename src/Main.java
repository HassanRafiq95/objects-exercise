public class Main
{

    public static void main(String[] args)
    {

        Pet marvin = new Pet("Marvin", "Dog");

        Person bob = new Person("Bob", "Smith", null);
	    Person sarah = new Person("Sarah", "Smith", marvin);

        sarah.setMyPet(marvin);
        sarah.setSurname("Jones");

        System.out.println(bob.getFullName());
        System.out.println(sarah.getFullName());

        Pet theirPet = sarah.getMyPet();

            if (theirPet == null)
            {
                System.out.println(sarah.getFirstName() + " has no pet");
            }
            else
            {
                System.out.println(sarah.getFirstName() + " has a pet called " + theirPet.getName());
            }


    }
}
